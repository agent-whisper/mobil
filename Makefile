all: task_1 task_2 task_3

task_3:
	g++ src/main_3.cpp -o main_3 -lglfw -lGLEW -lGL -lassimp -I src -g -std=c++11

task_2:
	g++ src/main_2.cpp -o bin/main_2 -lglfw -lGLEW -lGL
	g++ src/main_2_opengl2.cpp -o bin/main_2_opengl2 -lglut -lGL

task_1:
	g++ src/main_1.cpp -o bin/main_1 -lglfw -lGLEW -lGL
	g++ src/main_1_uniform.cpp -o bin/main_1_uniform -lglfw -lGLEW -lGL
	g++ src/main_1_opengl2.cpp -o bin/main_1_opengl2 -lglut -lGL
