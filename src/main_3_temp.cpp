#include <bits/stdc++.h>
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/transform.hpp>
#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

#include "main_3_mesh.hpp"

#include "window.hpp"

using namespace std;

void buildShaders(GLuint* vertexShader, GLuint* fragmentShader, GLuint* shaderProgram) {
    const char* vertexShaderSourceCode = "\n"
    "    #version 330 core\n"
    "    layout (location = 0) in vec3 aPos;\n"
    "    layout (location = 1) in vec3 aColor;\n"
    "    out vec3 color;\n"
    "    uniform mat4 transMat;\n"
    "    void main() {\n"
    "        color = aColor;\n"
    "        gl_Position = transMat * vec4(aPos.x, aPos.y, aPos.z, 1.0);\n"
    "    }\n";
    
    GLuint vs = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(vs, 1, &vertexShaderSourceCode, NULL);
    glCompileShader(vs);
    GLint success = 0;
    GLchar* info_log = (GLchar*) malloc(512);
    glGetShaderiv(vs, GL_COMPILE_STATUS, &success);
    if(!success) {
        glGetShaderInfoLog(vs, 512, NULL, info_log);
        printf("error: %s\n", info_log);
        exit(-1);
    }

    if (vertexShader)
        *vertexShader = vs;

    const char* fragmentShaderSourceCode = "\n"
    "   #version 330 core\n"
    "   in vec3 color;\n"
    "   out vec4 FragColor;\n"
    "   void main() {\n"
    "       FragColor = vec4(color, 1.0f);\n"
    "   }\n";

    GLuint fs = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(fs, 1, &fragmentShaderSourceCode, NULL);
    glCompileShader(fs);
    glGetShaderiv(fs, GL_COMPILE_STATUS, &success);
    if(!success) {
        glGetShaderInfoLog(fs, 512, NULL, info_log);
        printf("error: %s\n", info_log);
        exit(-1);
    }
    if (fragmentShader)
        *fragmentShader = fs;

    GLuint sp = glCreateProgram();
    glAttachShader(sp, vs);
    glAttachShader(sp, fs);
    glLinkProgram(sp);
    glGetProgramiv(sp, GL_LINK_STATUS, &success);
    if(!success) {
        glGetProgramInfoLog(sp, 512, NULL, info_log);
        printf("error: %s\n", info_log);
        exit(-1);
    }
    if (shaderProgram)
        *shaderProgram = sp;
}

void prepareVABO(float* vertices, GLuint size,
				 int* indices, GLuint idxSize,
				 GLuint* vertexBufferObject, GLuint *indexBufferObject, GLuint* vertexArrayObject,
				 int flag) {
    GLuint vbo;
    glGenBuffers(1, &vbo);
    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glBufferData(GL_ARRAY_BUFFER, size, vertices, flag);

    if (vertexBufferObject)
        *vertexBufferObject = vbo;

    GLuint ibo;
   	glGenBuffers(1, &ibo);
 	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibo);
 	glBufferData(GL_ELEMENT_ARRAY_BUFFER, idxSize, indices, GL_STATIC_DRAW);

 	if (indexBufferObject)
 		*indexBufferObject = ibo;

    GLuint vao;
    glGenVertexArrays(1, &vao);
    glBindVertexArray(vao);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), (void*)0);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), (void*)(3*sizeof(float)));
    glEnableVertexAttribArray(1);

    if (vertexArrayObject)
        *vertexArrayObject = vao;
}

void prepareVABO(float* vertices, GLuint size,
				 int* indices, GLuint idxSize,
				 GLuint* vertexBufferObject, GLuint* indexBufferObject, GLuint* vertexArrayObject) {
    prepareVABO(vertices, size, indices, idxSize, vertexBufferObject, indexBufferObject, vertexArrayObject, GL_STATIC_DRAW);
}

// void loadCar(Model& model) {
// 	Assimp::Importer importer;
// 	const aiScene *scene = importer.ReadFile("data/car/CHALLENGER71.3ds", aiProcess_Triangulate | aiProcess_FlipUVs);
// 	if(!scene || scene->mFlags & AI_SCENE_FLAGS_INCOMPLETE || !scene->mRootNode)  {
//        	cout<<"error loading car "<<importer.GetErrorString()<<endl;
//         exit(-1);
//     }

//     stack<aiNode*> nodeToProcess;
//     nodeToProcess.push(scene->mRootNode);

//     while (!nodeToProcess.empty()) {
//     	aiNode* node = nodeToProcess.top(); nodeToProcess.pop();
//     	for(int i = 0; i < (int) node->mNumChildren; i++)
//             nodeToProcess.push(node->mChildren[i]);

// 	    cout<<"num mesh : "<<node->mNumMeshes<<endl;
// 	    for(int i = 0; i < (int) node->mNumMeshes; i++) {
// 	        aiMesh *mesh = scene->mMeshes[node->mMeshes[i]];
// 	        // meshes.push_back(processMesh(mesh, scene));	
// 	        for (int j = 0; j < (int) mesh->mNumVertices; j++) {
// 	        	float x = mesh->mVertices[j].x;
// 	        	float y = mesh->mVertices[j].y;
// 	        	float z = mesh->mVertices[j].z;

// 	        	float nx = mesh->mNormals[j].x;
// 	        	float ny = mesh->mNormals[j].y;
// 	        	float nz = mesh->mNormals[j].z;

// 	        	float tx = 0, ty = 0;
// 	        	if (mesh->mTextureCoords[0]) {
// 				    float tx = mesh->mTextureCoords[0][j].x;
// 				    float ty = mesh->mTextureCoords[0][j].y;
// 				}

// 				// printf("%.2f %.2f %.2f | %.2f %.2f %.2f | %.2f %.2f\n", x, y, z, nx, ny, nz, tx, ty);

// 				model.vertices.push_back(glm::vec3(x,y,z));
// 				model.normals.push_back(glm::vec3(nx,ny,nz));
// 				model.uvs.push_back(glm::vec2(tx,ty));
// 	        }

// 	        for(int i = 0; i < (int) mesh->mNumFaces; i++) {
// 		    	aiFace face = mesh->mFaces[i];
// 		    	for(int j = 0; j < (int) face.mNumIndices; j++)
// 		        	;// indices.push_back(face.mIndices[j]);
// 			}

// 	        if (mesh->mMaterialIndex >= 0) {
// 			    aiMaterial *material = scene->mMaterials[mesh->mMaterialIndex];
// 			    // vector<Texture> diffuseMaps = loadMaterialTextures(material, 
// 			    //                                     aiTextureType_DIFFUSE, "texture_diffuse");
// 			    // textures.insert(textures.end(), diffuseMaps.begin(), diffuseMaps.end());
// 			    // vector<Texture> specularMaps = loadMaterialTextures(material, 
// 			    //                                     aiTextureType_SPECULAR, "texture_specular");
// 			    // textures.insert(textures.end(), specularMaps.begin(), specularMaps.end());
// 			}
// 	    }  
// 	}

//     // for(int i = 0; i < (int) node->mNumChildren; i++)
//         // ;// processNode(node->mChildren[i], scene);
// }

int main(int argc, char** argv) {
    Window window;
    glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

    // Model model;
    // loadCar(model);

    vector<float> vertices;
    vector<int> indices;
	FILE * file = fopen("data/car.obj", "r");
	if(file == NULL) {
		fprintf(stderr, "failed to open gile\n");
		return -1;
	}
	char line[256];
	int readstat = 1;
	while ((readstat = fscanf(file, "%s", line)) != EOF)
		if (strcmp(line, "v") == 0) {
			float x,y,z;
			fscanf(file, "%f%f%f", &x, &y, &z);
			vertices.push_back(x);
			vertices.push_back(y);
			vertices.push_back(z);
			vertices.push_back(0);
			vertices.push_back(0);
			vertices.push_back(0);
		} else if (strcmp(line, "f") == 0) {
			int a,b,c,d;
			fscanf(file, "%d/%d/%d %d/%d/%d %d/%d/%d", &a, &d, &d, &a, &d, &d, &c, &d, &d);
			indices.push_back(a-1);
			indices.push_back(b-1);
			indices.push_back(c-1);
		}

    GLuint vbo, vao, ibo;
    prepareVABO(&vertices[0], vertices.size() * sizeof(float), &indices[0], indices.size() * sizeof(int), &vbo, &ibo, &vao);

    GLuint vertexShader, fragmentShader, shaderProgram;
    buildShaders(&vertexShader, &fragmentShader, &shaderProgram);
    glUseProgram(shaderProgram);

    GLint vp[4];
    glGetIntegerv(GL_VIEWPORT, vp);
    float ratio = vp[3] * 1.0 / vp[2];

    // set view matrix
    glm::vec3 cameraPosition = glm::vec3(0.0f, 5.0f, 20.0f);
    glm::vec3 cameraTarget = glm::vec3(0.0f, 0.0f, 0.0f);
    glm::vec3 cameraDirection = glm::normalize(cameraTarget - cameraPosition);
    glm::vec3 cameraRight = glm::vec3(1.0f, 0.0f, 0.0f);
    glm::vec3 cameraUp = glm::cross(cameraRight, cameraDirection);
    glm::mat4 view = glm::lookAt(cameraPosition, cameraTarget, cameraUp);

    // set projection matrix
    glm::mat4 projection = glm::perspective(glm::radians(30.0f), ratio, 0.0f, 100.0f);
    GLuint matId = glGetUniformLocation(shaderProgram, "transMat");

    while(!window.shouldClose()) {
        glClearColor(0.8f, 0.8f, 0.8f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT);

        // set model matrix
        glm::mat4 rotation = glm::rotate(glm::mat4(), (float) glfwGetTime() * -0.5f, glm::vec3(0.0f, 1.0f, 0.0f));
        
        glm::mat4 mat = projection * view * rotation;
        glUniformMatrix4fv(matId, 1, GL_FALSE, &mat[0][0]);

        glBindVertexArray(vao);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibo);
        glDrawElements(GL_TRIANGLES, indices.size(), GL_UNSIGNED_INT, 0);

        window.swapBuffer();
        window.pollEvents();
    }

    glDeleteShader(vertexShader);
    glDeleteShader(fragmentShader);
    glfwTerminate();
    return 0;
}
