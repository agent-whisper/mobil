#include <bits/stdc++.h>
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/transform.hpp>
#include "window.hpp"

void buildShaders(GLuint* vertexShader, GLuint* fragmentShader, GLuint* shaderProgram) {
    const char* vertexShaderSourceCode = "\n"
    "    #version 330 core\n"
    "    layout (location = 0) in vec3 aPos;\n"
    "    layout (location = 1) in vec3 aColor;\n"
    "    uniform mat4 transMat;\n"
    "    out vec3 color;\n"
    "    void main() {\n"
    "        color = aColor;\n"
    "        gl_Position = transMat * vec4(aPos.x, aPos.y, aPos.z, 1.0);\n"
    "    }\n";
    
    GLuint vs = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(vs, 1, &vertexShaderSourceCode, NULL);
    glCompileShader(vs);
    GLint success = 0;
    GLchar* info_log = (GLchar*) malloc(512);
    glGetShaderiv(vs, GL_COMPILE_STATUS, &success);
    if(!success) {
        glGetShaderInfoLog(vs, 512, NULL, info_log);
        printf("error: %s\n", info_log);
        exit(-1);
    }

    if (vertexShader)
        *vertexShader = vs;

    const char* fragmentShaderSourceCode = "\n"
    "   #version 330 core\n"
    "   in vec3 color;\n"
    "   out vec4 FragColor;\n"
    "   void main() {\n"
    "       FragColor = vec4(color, 1.0f);\n"
    "   }\n";

    GLuint fs = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(fs, 1, &fragmentShaderSourceCode, NULL);
    glCompileShader(fs);
    glGetShaderiv(fs, GL_COMPILE_STATUS, &success);
    if(!success) {
        glGetShaderInfoLog(fs, 512, NULL, info_log);
        printf("error: %s\n", info_log);
        exit(-1);
    }
    if (fragmentShader)
        *fragmentShader = fs;

    GLuint sp = glCreateProgram();
    glAttachShader(sp, vs);
    glAttachShader(sp, fs);
    glLinkProgram(sp);
    glGetProgramiv(sp, GL_LINK_STATUS, &success);
    if(!success) {
        glGetProgramInfoLog(sp, 512, NULL, info_log);
        printf("error: %s\n", info_log);
        exit(-1);
    }
    if (shaderProgram)
        *shaderProgram = sp;
}

void prepareVABO(float* vertices, GLuint size, GLuint* vertexBufferObject, GLuint* vertexArrayObject, int flag) {
    GLuint vbo;
    glGenBuffers(1, &vbo);
    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glBufferData(GL_ARRAY_BUFFER, size, vertices, flag);

    if (vertexBufferObject)
        *vertexBufferObject = vbo;

    GLuint vao;
    glGenVertexArrays(1, &vao);
    glBindVertexArray(vao);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), (void*)0);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), (void*)(3*sizeof(float)));
    glEnableVertexAttribArray(1);

    if (vertexArrayObject)
        *vertexArrayObject = vao;
}

void prepareVABO(float* vertices, GLuint size, GLuint* vertexBufferObject, GLuint* vertexArrayObject) {
    prepareVABO(vertices, size, vertexBufferObject, vertexArrayObject, GL_STATIC_DRAW);
}

int main(int argc, char** argv) {
    Window window;

    float vertices[] = {
        94.278000, 33.722000, 0.0, 1.000000, 0.705882, 0.490196,
        54.522000, 80.102000, 0.0, 1.000000, 0.705882, 0.490196,
        21.011000, 95.515000, 0.0, 1.000000, 0.705882, 0.490196,
        -97.992000, 95.515000, 0.0, 1.000000, 0.705882, 0.490196,
        -132.462000, 82.764000, 0.0, 1.000000, 0.705882, 0.490196,
        -179.376000, 42.550000, 0.0, 1.000000, 0.705882, 0.490196,
        -229.415000, 35.402000, 0.0, 1.000000, 0.705882, 0.490196,
        -244.045000, 22.207000, 0.0, 1.000000, 0.705882, 0.490196,
        -249.995000, -1.587000, 0.0, 1.000000, 0.705882, 0.490196,
        -117.580000, -63.379000, 0.0, 1.000000, 0.705882, 0.490196,
        253.175000, -63.379000, 0.0, 1.000000, 0.705882, 0.490196,
        253.175000, -13.476000, 0.0, 1.000000, 0.705882, 0.490196,
        239.798000, 10.387000, 0.0, 1.000000, 0.705882, 0.490196,

        253.173000, -19.241000, 0.0, 1.000000, 0.568627, 0.392157,
        103.106000, 16.069000, 0.0, 1.000000, 0.568627, 0.392157,
        -241.169000, 16.069000, 0.0, 1.000000, 0.568627, 0.392157,
        -249.996000, 7.242000, 0.0, 1.000000, 0.568627, 0.392157,
        -249.996000, -45.723000, 0.0, 1.000000, 0.568627, 0.392157,
        -82.271000, -72.205000, 0.0, 1.000000, 0.568627, 0.392157,
        253.174000, -72.205000, 0.0, 1.000000, 0.568627, 0.392157,
        253.174000, -19.241000, 0.0, 1.000000, 0.568627, 0.392157,

        253.173000, -54.551000, 0.0, 1.000000, 0.490196, 0.352941,
        -160.756000, -54.551000, 0.0, 1.000000, 0.490196, 0.352941,
        -239.256000, -37.108000, 0.0, 1.000000, 0.490196, 0.352941,
        -249.791000, -43.810000, 0.0, 1.000000, 0.490196, 0.352941,
        -243.084000, -54.340000, 0.0, 1.000000, 0.490196, 0.352941,
        -163.637000, -71.995000, 0.0, 1.000000, 0.490196, 0.352941,
        -161.724000, -72.206000, 0.0, 1.000000, 0.490196, 0.352941,
        253.170000, -72.206000, 0.0, 1.000000, 0.490196, 0.352941,
        261.998000, -63.379000, 0.0, 1.000000, 0.490196, 0.352941,

        173.725000, -107.516000, 0.0, 0.313725, 0.294118, 0.352941,
        120.760000, -54.551000, 0.0, 0.313725, 0.294118, 0.352941,
        173.725000, -1.586000, 0.0, 0.313725, 0.294118, 0.352941,
        226.690000, -54.551000, 0.0, 0.313725, 0.294118, 0.352941,
        173.725000, -19.241000, 0.0, 0.313725, 0.294118, 0.352941,
        138.415000, -54.551000, 0.0, 0.313725, 0.294118, 0.352941,
        173.725000, -89.861000, 0.0, 0.313725, 0.294118, 0.352941,
        209.035000, -54.551000, 0.0, 0.313725, 0.294118, 0.352941,

        -161.721000, -107.516000, 0.0, 0.313725, 0.294118, 0.352941,
        -214.686000, -54.551000, 0.0, 0.313725, 0.294118, 0.352941,
        -161.721000, -1.586000, 0.0, 0.313725, 0.294118, 0.352941,
        -108.756000, -54.551000, 0.0, 0.313725, 0.294118, 0.352941,
        -161.721000, -19.241000, 0.0, 0.313725, 0.294118, 0.352941,
        -197.031000, -54.551000, 0.0, 0.313725, 0.294118, 0.352941,
        -161.721000, -89.861000, 0.0, 0.313725, 0.294118, 0.352941,
        -126.411000, -54.551000, 0.0, 0.313725, 0.294118, 0.352941,

        103.105000, -36.896000, 0.0, 1.000000, 0.490196, 0.352941,
        -82.272000, -36.896000, 0.0, 1.000000, 0.490196, 0.352941,
        -91.099000, -28.069000, 0.0, 1.000000, 0.490196, 0.352941,
        -91.099000, -28.069000, 0.0, 1.000000, 0.490196, 0.352941,
        -82.272000, -19.242000, 0.0, 1.000000, 0.490196, 0.352941,
        103.105000, -19.242000, 0.0, 1.000000, 0.490196, 0.352941,
        111.932000, -28.069000, 0.0, 1.000000, 0.490196, 0.352941,
        111.932000, -28.069000, 0.0, 1.000000, 0.490196, 0.352941,

        253.173000, -36.896000, 0.0, 1.000000, 0.490196, 0.352941,
        244.346000, -36.896000, 0.0, 1.000000, 0.490196, 0.352941,
        235.518000, -28.069000, 0.0, 1.000000, 0.490196, 0.352941,
        235.518000, -28.069000, 0.0, 1.000000, 0.490196, 0.352941,
        244.346000, -19.242000, 0.0, 1.000000, 0.490196, 0.352941,
        253.173000, -19.242000, 0.0, 1.000000, 0.490196, 0.352941,
        262.000000, -28.069000, 0.0, 1.000000, 0.490196, 0.352941,
        262.000000, -28.069000, 0.0, 1.000000, 0.490196, 0.352941,

        -179.376000, 42.551000, 0.0, 1.000000, 0.568627, 0.392157,
        -132.462000, 82.765000, 0.0, 1.000000, 0.568627, 0.392157,
        -97.992000, 95.516000, 0.0, 1.000000, 0.568627, 0.392157,
        21.011000, 95.516000, 0.0, 1.000000, 0.568627, 0.392157,
        54.522000, 80.103000, 0.0, 1.000000, 0.568627, 0.392157,
        94.278000, 33.723000, 0.0, 1.000000, 0.568627, 0.392157,
        -114.724000, 33.723000, 0.0, 1.000000, 0.568627, 0.392157,
        -137.945000, 35.645000, 0.0, 1.000000, 0.568627, 0.392157,

        11.294000, 80.336000, 0.0, 0.384314, 0.364706, 0.419608,
        -2.269000, 86.688000, 0.0, 0.384314, 0.364706, 0.419608,
        -83.793000, 86.688000, 0.0, 0.384314, 0.364706, 0.419608,
        -96.276000, 81.517000, 0.0, 0.384314, 0.364706, 0.419608,
        -122.758000, 55.035000, 0.0, 0.384314, 0.364706, 0.419608,
        -126.415000, 46.208000, 0.0, 0.384314, 0.364706, 0.419608,
        -126.415000, 46.208000, 0.0, 0.384314, 0.364706, 0.419608,
        -113.931000, 33.724000, 0.0, 0.384314, 0.364706, 0.419608,
        40.716000, 33.724000, 0.0, 0.384314, 0.364706, 0.419608,
        44.107000, 40.964000, 0.0, 0.384314, 0.364706, 0.419608,

        243.697000, 7.241000, 0.0, 0.929412, 0.929412, 0.933333,
        235.516000, 7.241000, 0.0, 0.929412, 0.929412, 0.933333,
        226.689000, -1.586000, 0.0, 0.929412, 0.929412, 0.933333,
        235.516000, -10.413000, 0.0, 0.929412, 0.929412, 0.933333,
        252.777000, -10.413000, 0.0, 0.929412, 0.929412, 0.933333,

        54.522000, 80.103000, 0.0, 0.384314, 0.364706, 0.419608,
        47.486000, 86.688000, 0.0, 0.384314, 0.364706, 0.419608,
        33.080000, 86.688000, 0.0, 0.384314, 0.364706, 0.419608,
        29.689000, 79.448000, 0.0, 0.384314, 0.364706, 0.419608,
        62.502000, 40.074000, 0.0, 0.384314, 0.364706, 0.419608,
        76.064000, 33.722000, 0.0, 0.384314, 0.364706, 0.419608,
        94.277000, 33.722000, 0.0, 0.384314, 0.364706, 0.419608,

        -249.996000, 7.241000, 0.0, 1.000000, 0.392157, 0.392157,
        -249.996000, -1.586000, 0.0, 1.000000, 0.392157, 0.392157,
        -232.341000, -1.586000, 0.0, 1.000000, 0.392157, 0.392157,
        -223.514000, 7.241000, 0.0, 1.000000, 0.392157, 0.392157,
        -232.341000, 16.068000, 0.0, 1.000000, 0.392157, 0.392157,
        -241.169000, 16.068000, 0.0, 1.000000, 0.392157, 0.392157,

        -64.619000, 33.724000, 0.0, 0.313725, 0.294118, 0.352941,
        -91.102000, 33.724000, 0.0, 0.313725, 0.294118, 0.352941,
        -73.447000, 86.688000, 0.0, 0.313725, 0.294118, 0.352941,
        -55.792000, 86.688000, 0.0, 0.313725, 0.294118, 0.352941
    };
    GLuint vbo, vao;
    prepareVABO(vertices, sizeof vertices, &vbo, &vao);

    float* vertices2 = new float[300*6];
    float pi = acos(-1);
    for (int i = 0; i < 100; i++) {
        float color = i / 10 & 1 ? 0.5 : 0.1;
        vertices2[i*18] = 0;
        vertices2[i*18+1] = 0;
        vertices2[i*18+2] = 0;
        vertices2[i*18+3] = color;
        vertices2[i*18+4] = color;
        vertices2[i*18+5] = color;

        vertices2[i*18+6] = 40.0*cos(2.0f*pi/100.0 * i);
        vertices2[i*18+7] = 40.0*sin(2.0f*pi/100.0 * i);
        vertices2[i*18+8] = 0;
        vertices2[i*18+9] = color;
        vertices2[i*18+10] = color;
        vertices2[i*18+11] = color;

        vertices2[i*18+12] = 40.0*cos(2.0f*pi/100.0 * (i+1));
        vertices2[i*18+13] = 40.0*sin(2.0f*pi/100.0 * (i+1));
        vertices2[i*18+14] = 0;
        vertices2[i*18+15] = color;
        vertices2[i*18+16] = color;
        vertices2[i*18+17] = color;
    }
    GLuint vbo2, vao2;
    prepareVABO(vertices2, sizeof(float)*300*6, &vbo2, &vao2);
    delete [] vertices2;

    GLuint vertexShader, fragmentShader, shaderProgram;
    buildShaders(&vertexShader, &fragmentShader, &shaderProgram);
    glUseProgram(shaderProgram);

    GLint vp[4];
    glGetIntegerv(GL_VIEWPORT, vp);
    float ratio = vp[3] * 1.0 / vp[2];
    float width = 600.0f;

    glm::mat4 view = glm::translate(glm::mat4(), glm::vec3(0.0f, 0.0f, -3.0f));
    glm::mat4 projection = glm::ortho(-width/2.0f, width/2.0f, -ratio * width / 2.0f, ratio * width / 2.0f, 6.0f, -6.0f);
    GLuint matId = glGetUniformLocation(shaderProgram, "transMat");

    while(!window.shouldClose()) {
        glClearColor(0.8f, 0.8f, 0.8f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT);

        glm::mat4 mat = projection * view;
        glUniformMatrix4fv(matId, 1, GL_FALSE, &mat[0][0]);

        glBindVertexArray(vao);
        glDrawArrays(GL_TRIANGLE_FAN, 0, 13);
        glDrawArrays(GL_TRIANGLE_FAN, 13, 8);
        glDrawArrays(GL_TRIANGLE_FAN, 21, 9);
        // glDrawArrays(GL_TRIANGLE_FAN, 30, 8);
        // glDrawArrays(GL_TRIANGLE_FAN, 38, 8);
        glDrawArrays(GL_TRIANGLE_FAN, 46, 8);
        glDrawArrays(GL_TRIANGLE_FAN, 54, 8);
        glDrawArrays(GL_TRIANGLE_FAN, 62, 8);
        glDrawArrays(GL_TRIANGLE_FAN, 70, 10);
        glDrawArrays(GL_TRIANGLE_FAN, 80, 5);
        glDrawArrays(GL_TRIANGLE_FAN, 85, 7);
        glDrawArrays(GL_TRIANGLE_FAN, 92, 6);
        glDrawArrays(GL_TRIANGLE_FAN, 98, 4);

        // draw front tire
        glm::mat4 rotation = glm::rotate(glm::mat4(), (float) glfwGetTime() * -10.0f, glm::vec3(0.0f, 0.0f, 1.0f));
        glm::mat4 model = glm::translate(glm::mat4(), glm::vec3(170.0f, -60.0f, 0.0f));
        mat = projection * view * model * rotation;
        glUniformMatrix4fv(matId, 1, GL_FALSE, &mat[0][0]);

        glBindVertexArray(vao2);
        glDrawArrays(GL_TRIANGLES, 0, 300);

        // draw back tire

        model = glm::translate(glm::mat4(), glm::vec3(-150.0f, -60.0f, 0.0f));
        mat = projection * view * model * rotation;
        glUniformMatrix4fv(matId, 1, GL_FALSE, &mat[0][0]);

        glBindVertexArray(vao2);
        glDrawArrays(GL_TRIANGLES, 0, 300);

        window.swapBuffer();
        window.pollEvents();
    }

    glDeleteShader(vertexShader);
    glDeleteShader(fragmentShader);
    glfwTerminate();
    return 0;
}