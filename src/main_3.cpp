#include <bits/stdc++.h>
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/transform.hpp>

#include "main_3_mesh.hpp"
#include "main_3_shader.hpp"
#include "main_3_model.hpp"

using namespace std;

bool mouseClicked = false;
bool modelRotation = true;
float modelRotationAngle = 0.0;
double clickedX = 0.0;
double clickedY = 0.0;
double rotateX = 0.0;
double rotateY = 0.0;
float frontOffset = 0.0;

void framebufferSizeCallback(GLFWwindow* window, int width, int height);
void mouseButtonCallback(GLFWwindow* window, int button, int action, int mods);
void mouseCallback(GLFWwindow* window, double xpos, double ypos);
void scrollCallback(GLFWwindow* window, double xoffset, double yoffset);
void keyCallback(GLFWwindow* window, int key, int scancode, int action, int mods);

// screen settings
const unsigned int SCR_WIDTH = 1024;
const unsigned int SCR_HEIGHT = 768;

// Camera, Mouse, and Scroll
float lastX = SCR_WIDTH / 2.0f;
float lastY = SCR_HEIGHT / 2.0f;
bool firstMouse = true;

// fariz - set lighting position
glm::vec3 lightPos(300.0f, 200.0f, 2.0f);


int main(int argc, char** argv) {
	// init window
    if (!glfwInit()) {
        fprintf(stderr, "failed to initialize glfw\n");
        exit(-1);
    }

    glfwWindowHint(GLFW_SAMPLES, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

    GLFWwindow* window = glfwCreateWindow( SCR_WIDTH, SCR_HEIGHT, "Task 3", NULL, NULL);
    if(!window){
        fprintf( stderr, "Failed to open GLFW window. If you have an Intel GPU, they are not 3.3 compatible. Try the 2.1 version of the tutorials.\n" );
        glfwTerminate();
        exit(-1);
    }

    glfwSetFramebufferSizeCallback(window, framebufferSizeCallback);
    glfwSetKeyCallback(window, keyCallback);
    glfwSetMouseButtonCallback(window, mouseButtonCallback);
    glfwSetCursorPosCallback(window, mouseCallback);
    glfwSetScrollCallback(window, scrollCallback);
    glfwMakeContextCurrent(window);

    glewExperimental = true;
    if (glewInit() != GLEW_OK) {
        fprintf(stderr, "Failed to initialize GLEW\n");
        exit(-1);
    }

    // init shaders
    Shader shader("src/main_3_vertex_shader.vs", "src/main_3_fragment_shader.fs");
    // fariz - compile light shaders
    Shader lightingShader("src/main_3_light_shader.vs", "src/main_3_light_shader.fs");
    Shader lampShader("src/main_3_lamp_shader.vs", "src/main_3_lamp_shader.fs");


    // enable depth buffer
    glEnable(GL_DEPTH_TEST);

    // Specify a global ambient
    GLfloat globalAmbient[] = { 0.2, 0.2, 0.2, 1.0 };
    glLightModelfv( GL_LIGHT_MODEL_AMBIENT, globalAmbient );

    glEnable(GL_LIGHTING);

    // inti model
    Model model("data/car/car.obj");

    glTexEnvi(GL_TEXTURE_ENV,GL_TEXTURE_ENV_MODE,GL_MODULATE);
    // fariz - box vertices
    float box_vertices[] = {
        -0.5f, -0.5f, -0.5f,  0.0f,  0.0f, -1.0f,
         0.5f, -0.5f, -0.5f,  0.0f,  0.0f, -1.0f,
         0.5f,  0.5f, -0.5f,  0.0f,  0.0f, -1.0f,
         0.5f,  0.5f, -0.5f,  0.0f,  0.0f, -1.0f,
        -0.5f,  0.5f, -0.5f,  0.0f,  0.0f, -1.0f,
        -0.5f, -0.5f, -0.5f,  0.0f,  0.0f, -1.0f,

        -0.5f, -0.5f,  0.5f,  0.0f,  0.0f,  1.0f,
         0.5f, -0.5f,  0.5f,  0.0f,  0.0f,  1.0f,
         0.5f,  0.5f,  0.5f,  0.0f,  0.0f,  1.0f,
         0.5f,  0.5f,  0.5f,  0.0f,  0.0f,  1.0f,
        -0.5f,  0.5f,  0.5f,  0.0f,  0.0f,  1.0f,
        -0.5f, -0.5f,  0.5f,  0.0f,  0.0f,  1.0f,

        -0.5f,  0.5f,  0.5f, -1.0f,  0.0f,  0.0f,
        -0.5f,  0.5f, -0.5f, -1.0f,  0.0f,  0.0f,
        -0.5f, -0.5f, -0.5f, -1.0f,  0.0f,  0.0f,
        -0.5f, -0.5f, -0.5f, -1.0f,  0.0f,  0.0f,
        -0.5f, -0.5f,  0.5f, -1.0f,  0.0f,  0.0f,
        -0.5f,  0.5f,  0.5f, -1.0f,  0.0f,  0.0f,

         0.5f,  0.5f,  0.5f,  1.0f,  0.0f,  0.0f,
         0.5f,  0.5f, -0.5f,  1.0f,  0.0f,  0.0f,
         0.5f, -0.5f, -0.5f,  1.0f,  0.0f,  0.0f,
         0.5f, -0.5f, -0.5f,  1.0f,  0.0f,  0.0f,
         0.5f, -0.5f,  0.5f,  1.0f,  0.0f,  0.0f,
         0.5f,  0.5f,  0.5f,  1.0f,  0.0f,  0.0f,

        -0.5f, -0.5f, -0.5f,  0.0f, -1.0f,  0.0f,
         0.5f, -0.5f, -0.5f,  0.0f, -1.0f,  0.0f,
         0.5f, -0.5f,  0.5f,  0.0f, -1.0f,  0.0f,
         0.5f, -0.5f,  0.5f,  0.0f, -1.0f,  0.0f,
        -0.5f, -0.5f,  0.5f,  0.0f, -1.0f,  0.0f,
        -0.5f, -0.5f, -0.5f,  0.0f, -1.0f,  0.0f,

        -0.5f,  0.5f, -0.5f,  0.0f,  1.0f,  0.0f,
         0.5f,  0.5f, -0.5f,  0.0f,  1.0f,  0.0f,
         0.5f,  0.5f,  0.5f,  0.0f,  1.0f,  0.0f,
         0.5f,  0.5f,  0.5f,  0.0f,  1.0f,  0.0f,
        -0.5f,  0.5f,  0.5f,  0.0f,  1.0f,  0.0f,
        -0.5f,  0.5f, -0.5f,  0.0f,  1.0f,  0.0f
    };

    // fariz - Setup light vao and vbo
    unsigned int lightVBO, lightVAO;
    glGenVertexArrays(1, &lightVAO);
    glGenBuffers(1, &lightVBO);

    glBindBuffer(GL_ARRAY_BUFFER, lightVBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(box_vertices), box_vertices, GL_STATIC_DRAW);

    glBindVertexArray(lightVAO);

    // position attribute
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), (void*)0);
    glEnableVertexAttribArray(0);
    // normal attribute
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), (void*)(3 * sizeof(float)));
    glEnableVertexAttribArray(1);

    while(!glfwWindowShouldClose(window)) {
        glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        // fariz - enable light shader
        // be sure to activate shader when setting uniforms/drawing objects
        lightingShader.use();
        lightingShader.setVec3("objectColor", 1.0f, 0.5f, 0.31f);
        lightingShader.setVec3("lightColor", 1.0f, 1.0f, 1.0f);
        lightingShader.setVec3("lightPos", lightPos);

        // set view matrix
        glm::vec3 cameraPosition = glm::vec3(0.0f, 20.0f, 40.0f);
        glm::vec3 cameraTarget = glm::vec3(0.0f, 0.0f, 0.0f);
        glm::vec3 cameraDirection = glm::normalize(cameraTarget - cameraPosition);
        glm::vec3 cameraRight = (glm::cross(glm::vec3(-cameraPosition.x,0,-cameraPosition.z), glm::vec3(0,1,0)));
        glm::vec3 cameraUp = glm::cross(cameraRight, cameraDirection);

        // set front offset
        cameraPosition += (float) frontOffset * cameraDirection;

        // set | rotation
        glm::mat4 cameraRotationY = glm::rotate(glm::mat4(), (float) glm::radians((float) -rotateY/1.0), cameraUp);
        cameraPosition = glm::vec3(cameraRotationY * glm::vec4(cameraPosition, 1.0));
        cameraDirection = glm::normalize(cameraTarget - cameraPosition);
        cameraRight = glm::cross(cameraDirection, cameraUp);

        // set - rotation
        glm::mat4 cameraRotationX = glm::rotate(glm::mat4(), (float) glm::radians((float) -rotateX/1.0), cameraRight);
        cameraPosition = glm::vec3(cameraRotationX * glm::vec4(cameraPosition, 1.0));
        cameraDirection = glm::normalize(cameraTarget - cameraPosition);
        cameraUp = glm::cross(cameraRight, cameraDirection);

        glm::mat4 view = glm::lookAt(cameraPosition, cameraTarget, cameraUp);

        // lightingShader.setVec3("viewPos", cameraPosition); // add camera position
        // get ration
        GLint vp[4];
        glGetIntegerv(GL_VIEWPORT, vp);
        float ratio = vp[2] * 1.0 / vp[3];

        // set projection matrix
        glm::mat4 projection = glm::perspective(glm::radians(30.0f), ratio, 0.1f, 100.0f);

        // set model matrix
        static float timeOffset = 0.0;
        if (modelRotation)
            modelRotationAngle = (float) (glfwGetTime() - timeOffset);
        else
            timeOffset = (float) (glfwGetTime() - modelRotationAngle);
        glm::mat4 rotation = glm::rotate(glm::mat4(), modelRotationAngle * -0.5f, glm::vec3(0.0f, 1.0f, 0.0f));

        // fariz - set light projection and view matrix
        lightingShader.setMat4("projection", projection);
        lightingShader.setMat4("view", view);

        // world transformation
        glm::mat4 lightModel;
        // lightingShader.setMat4("model", lightModel);
        lightingShader.setMat4("model", rotation);

        glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
        shader.use();
        shader.setVec3("lightColor", 1.0f, 1.0f, 1.0f);
        shader.setVec3("lightPos", lightPos);
        shader.setMat4("projection", projection);
        shader.setMat4("view", view);
        shader.setMat4("model", rotation);
        model.Draw(shader);

        // also draw the lamp object
        lampShader.use();
        lampShader.setMat4("projection", projection);
        lampShader.setMat4("view", view);
        lightModel = glm::mat4();
        lightModel = glm::translate(lightModel, lightPos);
        lightModel = glm::scale(lightModel, glm::vec3(0.2f)); // a smaller cube
        lampShader.setMat4("model", lightModel);

        glBindVertexArray(lightVAO);
        glDrawArrays(GL_TRIANGLES, 0, 36);

        glfwSwapBuffers(window);
        glfwPollEvents();
    }
    glfwTerminate();
    return 0;
}

void framebufferSizeCallback(GLFWwindow* window, int width, int height) {
    glViewport(0, 0, width, height);
}

void mouseButtonCallback(GLFWwindow* window, int button, int action, int mods) {
    if (button == GLFW_MOUSE_BUTTON_LEFT && action == GLFW_PRESS) {
        mouseClicked = true;
        glfwGetCursorPos(window, &clickedX, &clickedY);
    }

    if (button == GLFW_MOUSE_BUTTON_LEFT && action == GLFW_RELEASE) {
        mouseClicked = false;
    }
}

void mouseCallback(GLFWwindow* window, double xpos, double ypos) {
    static double offsetX = 0.0;
    static double offsetY = 0.0;
    if (mouseClicked) {
        rotateY = offsetY + xpos - clickedX;
        rotateX = offsetX + ypos - clickedY;
    } else {
        offsetX = rotateX;
        offsetY = rotateY;
    }
}

void scrollCallback(GLFWwindow* window, double xoffset, double yoffset) {
    printf("Scroll gerak\n");
}

void keyCallback(GLFWwindow* window, int key, int scancode, int action, int mods) {
    static bool w_pressed = false;
    static bool s_pressed = false;

    if (key == GLFW_KEY_W)
        w_pressed = action == GLFW_PRESS ? 1 : (action == GLFW_RELEASE ? 0 : w_pressed);
    else if (key == GLFW_KEY_S)
        s_pressed = action == GLFW_PRESS ? 1 : (action == GLFW_RELEASE ? 0 : s_pressed);
    
    if (w_pressed)
        frontOffset += 1.0;
    else if (s_pressed)
        frontOffset -= 1.0;

    if (key == GLFW_KEY_SPACE && action == GLFW_PRESS)
        modelRotation = !modelRotation;
}