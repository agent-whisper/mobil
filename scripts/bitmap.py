#!/usr/bin/python3

from PIL import Image
import sys

if len(sys.argv) < 2:
    print("Usage: bitmap.py filename")
    sys.exit(-1)

filename = sys.argv[1]

im = Image.open(filename, 'r')
pic = im.load()

width, height = im.size

print("int width = %d;" % width)
print("int height = %d;" % height)

data = list(im.getdata())
pixels = []
for item in data:
    r, g, b = item[:3]
    a = 255
    if len(item) > 3:
        a = item[3]
    print("%d %d %d %d" % (r,g,b,a))